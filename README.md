## Create Function

```shell
pulsar-admin functions create --jar $PWD/target/pulsar-functions-1.0-SNAPSHOT.nar --function-config-file $PWD/function-config.yaml
```

## Create topics

```shell
pulsar-admin topics create persistent://manning/ashish/pulsar-func-in
pulsar-admin topics create persistent://manning/ashish/pulsar-func-out
pulsar-admin topics create persistent://manning/ashish/pulsar-func-log
```

## List function

```shell
pulsar-admin functions list --tenant manning --namespace ashish
```

```http request
http://localhost:8080/admin/v3/functions/manning/ashish/keyword-filter
```

## Info

```http request
#http://localhost:8080/admin/v3/functions/manning/ashish/keyword-filter
http://localhost:8080/admin/v3/functions/manning/ashish/keyword-filter/0/status
```

## Start Consumer

```shell
pulsar-client consume persistent://manning/ashish/pulsar-func-out --subscription-name pulsar-func-out-cmd-consumer --subscription-type Exclusive -n 0
```
