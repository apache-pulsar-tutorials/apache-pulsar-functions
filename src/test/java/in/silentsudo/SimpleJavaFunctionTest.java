package in.silentsudo;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class SimpleJavaFunctionTest {

    private SimpleJavaFunction simpleJavaFunction = new SimpleJavaFunction();


    @Test
    public void simpleNotNullTest() throws Exception {
        final String sentence = "The brown fox jumped over the lazy dog";
        final String result = simpleJavaFunction.apply(sentence);
        Assertions.assertNotNull(result);
    }

    @Test
    public void exceptionTest() throws Exception {
        Assertions.assertThrows(IllegalArgumentException.class, () -> simpleJavaFunction.apply(null));
    }

    @Test
    public void exceptionTestMessage() throws Exception {
        IllegalArgumentException illegalArgumentException = Assertions.assertThrows(IllegalArgumentException.class, () -> simpleJavaFunction.apply(null));

        Assertions.assertEquals(SimpleJavaFunction.EXCEPTION_MSG, illegalArgumentException.getMessage());
    }

}
