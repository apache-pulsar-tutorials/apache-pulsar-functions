package in.silentsudo;

import org.apache.pulsar.functions.api.Context;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.Optional;

public class SimplePulsarFunctionTest {

    private SimplePulsarFunction simplePulsarFunction = new SimplePulsarFunction();

    @Mock
    private Context mockedContext;

    @BeforeEach
    public final void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void nullKeyWordTest() throws Exception {
        Mockito.when(mockedContext.getUserConfigValue(SimplePulsarFunction.KEYWORD_CONFIG))
                .thenReturn(Optional.empty());

        String sentence = "The brown fox jumped over the lazy dog";

        String result = simplePulsarFunction.process(sentence, mockedContext);
        Assertions.assertNull(result);
    }

    @Test
    public void containsKeyWordTest() throws Exception {
        Mockito.when(mockedContext.getUserConfigValue(SimplePulsarFunction.KEYWORD_CONFIG))
                .thenReturn(Optional.of("dog"));

        String sentence = "The brown fox jumped over the lazy dog";

        String result = simplePulsarFunction.process(sentence, mockedContext);
        Assertions.assertNotNull(result);
        Assertions.assertEquals(sentence, result);

    }

    @Test
    public void doesNotContainsKeyWordTest() throws Exception {
        Mockito.when(mockedContext.getUserConfigValue(SimplePulsarFunction.KEYWORD_CONFIG))
                .thenReturn(Optional.of("cat"));

        String sentence = "It was the best of times, it was the worst of times";

        String result = simplePulsarFunction.process(sentence, mockedContext);
        Assertions.assertNull(result);

    }

    @Test
    public void ignoreCaseKeyWordTest() throws Exception {
        Mockito.when(mockedContext.getUserConfigValue(SimplePulsarFunction.KEYWORD_CONFIG))
                .thenReturn(Optional.of("DOG"));
        Mockito.when(mockedContext.getUserConfigValue(SimplePulsarFunction.IGNORE_CASE))
                .thenReturn(Optional.of(Boolean.TRUE));

        String sentence = "The brown fox jumped over the lazy dog";

        String result = simplePulsarFunction.process(sentence, mockedContext);
        Assertions.assertNotNull(result);
        Assertions.assertEquals(sentence, result);
    }
}
