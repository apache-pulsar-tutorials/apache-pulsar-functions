package in.silentsudo;

import org.apache.pulsar.functions.api.Context;
import org.apache.pulsar.functions.api.Function;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class SimplePulsarFunction implements Function<String, String> {
    public static final String KEYWORD_CONFIG = "keyword";
    public static final String IGNORE_CASE = "ignore-case";


    @Override
    public String process(String input, Context context) throws Exception {
        Optional<Object> keyword = context.getUserConfigValue(KEYWORD_CONFIG);
        Optional<Object> ignoreConfig = context.getUserConfigValue(IGNORE_CASE);
        boolean ignoreCase = ignoreConfig.isPresent() ? (boolean) ignoreConfig.get() : false;

        final List<String> words = Arrays.asList(input.split("\\s"));

        if (!keyword.isPresent()) {
            return null;
        } else if (ignoreCase && words.stream().anyMatch(s -> s.equalsIgnoreCase((String) keyword.get()))) {
            return input;
        } else if (words.contains(keyword.get())) {
            return input;
        }

        return null;
    }
}
