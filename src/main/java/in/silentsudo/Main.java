//package in.silentsudo;
//
//import org.apache.pulsar.client.api.*;
//import org.apache.pulsar.common.functions.ConsumerConfig;
//import org.apache.pulsar.common.functions.FunctionConfig;
//import org.apache.pulsar.functions.LocalRunner;
//
//import java.util.List;
//import java.util.Map;
//import java.util.concurrent.ExecutorService;
//import java.util.concurrent.Executors;
//
//public class Main {
//    public static final String BROKER_URL = "pulsar://localhost:6650";
//    public static final String IN = "persistent://manning/ashish/pulsar-func-in";
//    public static final String OUT = "persistent://manning/ashish/pulsar-func-out";
//
//    private static ExecutorService executorService;
//    private static LocalRunner localRunner;
//    private static PulsarClient pulsarClient;
//    private static Producer<String> producer;
//    private static Consumer<String> consumer;
//
//    private static String keyword = "";
//
//    public static void main(String[] args) throws Exception {
//        System.out.println("Starting  Pulsar function Integration Testing...");
//        startLocalRunner();
//        init();
//        startConsumer();
//    }
//
//    public static void init() throws PulsarClientException {
//        executorService = Executors.newFixedThreadPool(2);
//        pulsarClient = PulsarClient.builder()
//                .serviceUrl(BROKER_URL)
//                .build();
//
//        producer = pulsarClient.newProducer(Schema.STRING).topic(IN).create();
//        consumer = pulsarClient.newConsumer(Schema.STRING).topic(OUT).subscriptionName("validation-sub").subscribe();
//    }
//
//    public static void startLocalRunner() throws Exception {
//        localRunner = LocalRunner.builder()
//                .brokerServiceUrl(BROKER_URL)
//                .functionConfig(getFunctionConfig())
//                .build();
//        localRunner.start(false);
//    }
//
//    public static FunctionConfig getFunctionConfig() {
//        ConsumerConfig consumerConfig = ConsumerConfig.builder().schemaType(Schema.STRING.getSchemaInfo().getName()).build();
//        Map<String, ConsumerConfig> inputSpecs = Map.of(IN, consumerConfig);
//
//        Map<String, Object> userConfig = Map.of(
//                SimplePulsarFunction.KEYWORD_CONFIG, keyword,
//                SimplePulsarFunction.IGNORE_CASE, true
//        );
//
//        return FunctionConfig
//                .builder()
//                .className(SimplePulsarFunction.class.getName())
//                .inputs(List.of(IN))
//                .inputSpecs(inputSpecs)
//                .output(OUT)
//                .name("simple-keywords-filter")
//                .tenant("manning")
//                .namespace("manning/ashish")
//                .runtime(FunctionConfig.Runtime.JAVA)
//                .subName("keyword-filtered-subscription")
//                .userConfig(userConfig)
//                .build();
//    }
//
//    public static void startConsumer() {
//        Runnable consumerRunnable= () -> {
//            while (consumer.isConnected()) {
//                Message<String> message = null;
//                try {
//                    message = consumer.receive();
//                    if (message != null) {
//                        System.out.println("Message received: " + message);
//                        consumer.acknowledge(message);
//                    }
//                } catch (PulsarClientException e) {
//                    throw new RuntimeException(e);
//                }
//            }
//        };
//        executorService.submit(consumerRunnable);
//    }
//
//
//}
