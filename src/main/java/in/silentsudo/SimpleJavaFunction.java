package in.silentsudo;

import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.function.Function;

public class SimpleJavaFunction implements Function<String, String> {
    public static final String EXCEPTION_MSG = "Input cannot be null";

    @Override
    public String apply(String s) {
        if (s != null) {
            final String date = OffsetDateTime.now().format(DateTimeFormatter.RFC_1123_DATE_TIME);
            final String output = s + ", Modified in function: " + date;
            return output;
        } else {
            throw new IllegalArgumentException(EXCEPTION_MSG);
        }
    }
}
