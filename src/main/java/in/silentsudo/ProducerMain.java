package in.silentsudo;

import org.apache.pulsar.client.api.Producer;
import org.apache.pulsar.client.api.PulsarClient;
import org.apache.pulsar.client.api.Schema;

import java.io.*;
import java.nio.charset.StandardCharsets;

public class ProducerMain {

    /**
     * //Consumer for this producer
     * pulsar-client consume \
     * persistent://manning/ashish/pulsar-learning --num-messages 0 \
     * --subscription-name pulsar-learning-subscription \
     * --subscription-type Exclusive
     */

    public static void main(String[] args) throws IOException {
        PulsarClient pulsarClient = PulsarClient.builder().serviceUrl("pulsar://localhost:6650").build();
        InputStreamReader inputStreamReader = new InputStreamReader(new FileInputStream(new File("test-data.txt")), StandardCharsets.UTF_8);
        BufferedReader reader = new BufferedReader(inputStreamReader);
        String line = null;
        Producer<String> producer = pulsarClient.newProducer(Schema.STRING)
                .topic("persistent://manning/ashish/pulsar-func-in").create();
        while ((line = reader.readLine()) != null) {
            System.out.println(line);
            producer.send(line);
        }
    }
}
